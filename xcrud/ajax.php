<?php class Ajax extends Controller{
        public function __construct(){
            parent::__construct();
        }
        public function index(){
            include(FCPATH.'/xcrud/xcrud.php');
            Xcrud::import_session($this->session->userdata('xcrud_sess'));
            Xcrud::get_requested_instance();
            $this->session->set_userdata(array('xcrud_sess'=>Xcrud::export_session()));
        }
    } 
<?php

class Create_category extends Controller {

	public function __construct() {
		parent::Controller();
		
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
	}

	public function index() {
		$this->load->view('create_category');
	}
	public function submit() {

		if ($this->_submit_validate() === FALSE) {
			$this->index();
			return;
		}
		$p = new CategoryTable();
		$p->CategoryName = $this->input->post('name');
		$p->SrNo = $this->input->post('srno');
		$p->Publish = $this->input->post('publish');
		$p->save();
		$this->load->view('submit_success');

	}

	private function _submit_validate() {

		// validation rules
		$this->form_validation->set_rules('name', 'Category Name',
			'required|alpha_numeric_spaces|min_length[1]|max_length[20]|unique[CategoryTable.categoryname]');
		
		$this->form_validation->set_rules('srno', 'SrNo',
			'required|numeric|min_length[1]|max_length[15]');
		
		return $this->form_validation->run();

	}
}

<?php

class Create_product extends Controller {

	public function __construct() {
		parent::Controller();
		
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');
	}

	public function index() {
		$this->load->view('create_product');
	}
	public function submit() {

		if ($this->_submit_validate() === FALSE) {
			$this->index();
			return;
		}
		$p = new ProductTable();
		$c = new ProdCatAssTable();
		$p->Name = $this->input->post('name');
		$p->SKU = $this->input->post('sku');
		$p->Publish = $this->input->post('publish');
		$p->Brief_Description = $this->input->post('brief_description');
		$p->save();
		$c->fkCategoryId = $this->input->post('cat_name');
		$c->fkProductId = $p->pkProductId;
		$c->save();
		$this->load->view('submit_success');

	}

	private function _submit_validate() {

		// validation rules
		$this->form_validation->set_rules('name', 'Product Name',
			'required|alpha_numeric_spaces|min_length[6]|max_length[20]|unique[ProductTable.name]');
		
		$this->form_validation->set_rules('sku', 'SKU',
			'required|numeric|min_length[6]|max_length[15]');
		
		$this->form_validation->set_rules('brief_description', 'Brief Description',
			'required|alpha_numeric_spaces|min_length[6]|max_length[100]');
		
		return $this->form_validation->run();

	}
}

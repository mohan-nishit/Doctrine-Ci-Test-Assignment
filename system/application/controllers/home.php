<?php
class Home extends Controller {

	public function index() {
//fetch categories and products
	$this->load->helper(array('url'));
		$vars['categories'] = Doctrine::getTable('CategoryTable')->findAll();
		$vars['products'] = Doctrine::getTable('ProductTable')->findAll();
		$vars['catass'] = Doctrine::getTable('ProdCatAssTable')->findAll();
		//load view with all arrays
		$vars['title'] = 'Home';
        $vars['content_view'] = 'category_list';
		$vars['container_css'] = 'categories';
		$this->load->view('template', $vars);
	}	

}

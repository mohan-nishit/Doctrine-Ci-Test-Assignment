<?php
class Categories extends Controller {

	public function display($id) {
		//find category from cat id
		$category = Doctrine::getTable('CategoryTable')->find($id);
		$vars['title'] = $category['CategoryName'];
		//find category assigned using cat id
		$catass = Doctrine::getTable('ProdCatAssTable')->findByFkCategoryId($id);
		//find all matching product ids and load into products array
		foreach($catass as $catassid){
		$prod_id = $catassid->fkProductId;
		$vars['products'][] = Doctrine::getTable('ProductTable')->findOneByPkProductId($prod_id);
		}
		
		//load view and template variables
		$vars['content_view'] = 'catass_list';
		$vars['container_css'] = 'category';
		$vars['catass'] = $catass;
		$vars['category'] = $category;
		$this->load->view('template', $vars);

	}

}

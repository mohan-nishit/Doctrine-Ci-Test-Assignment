<?php
class Products extends Controller {

	public function display($id) {
		//fetch product from product id
		$product = Doctrine::getTable('ProductTable')->find($id);
		//find category assigned
		$catass = Doctrine::getTable('ProdCatAssTable')->findByFkProductId($id);
		$cat = $catass->toArray();
		//print_r($cat[0]['fkCategoryId']);
		//get category from category table
		$category = Doctrine::getTable('CategoryTable')->find($cat[0]['fkCategoryId']);
		//get images using product id
		$images = Doctrine::getTable('ProductImagesTable')->findByFkProductId($id);
		$vars['title'] = $product['Name'];
		//load template vars
		$vars['content_view'] = 'product_list';
		$vars['container_css'] = 'category';
		//load required vars
		$vars['catass'] = $catass;
		$vars['category'] = $category;
		$vars['product'] = $product;
		$vars['images'] = $images;
		//load template view
		$this->load->view('template', $vars);

	}

}

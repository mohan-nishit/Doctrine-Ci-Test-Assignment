<?php
class Test extends Controller {

	function index() {

		$catass = new ProdCatAssTable();
		$catass->SrNo = "001";
		$catass->save();

		$image1 = new ProductImagesTable();
		$image1->ImageFileName = "001.jpg";
		
		
		$image2 = new ProductImagesTable();
		$image2->ImageFileName = "002.jpg";
		

		$product = new ProductTable();
		$product->Name = "Product1";
		$product->Brief_Description = "Use this Product!";

		$product->ProdCatAssTable = $catass;
		$product->Images[0] = $image1;
		$product->Images[1] = $image2;
		
		$category = new CategoryTable();
		$category->pkCatId = $catass->fkCategoryId;
		$category->CategoryName = "test".$catass->fkCategoryId;
		$category->SEODescription = "Blah Blah Blah Test";

		$conn = Doctrine_Manager::connection();
	    $conn->flush();
	}

}

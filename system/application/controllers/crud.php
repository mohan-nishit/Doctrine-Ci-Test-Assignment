<?php class Crud extends Controller{
        public function __construct(){
            parent::__construct();
        }
		public function products() {
			include(FCPATH.'/xcrud/xcrud.php');
            $xcrud = Xcrud::get_instance();
            $xcrud->table('product');
			$images = $xcrud->nested_table('Product Images','pkproductid','images','fkproductid');
			$images->change_type('imagefilename','image','',array('manual_crop'=>true,'not_rename'=>true));
			$images->modal('imagefilename');
			//$xcrud->unset_add();
			$data['content'] = $xcrud->render();
			$this->load->view('view_products', $data);
		}
		public function categories() {
			include(FCPATH.'/xcrud/xcrud.php');
            $xcrud = Xcrud::get_instance();
            $xcrud->table('categories');
			$catass = $xcrud->nested_table('Category Assignment','pkcatid','prodcatasstable','fkcategoryid');
			$products = $catass->nested_table('Products in Category','fkproductid','product','pkproductid');
			//$xcrud->unset_add();
			$data['content'] = $xcrud->render();
			$this->load->view('view_products', $data);
		}
    } 
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Home</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css" type="text/css" media="all">
</head>
<body>

<div class="categories">

	<h1>Categories</h1>

	<?php foreach($categories as $category): ?>

		<div class="category">
			<h2><?php echo $category->CategoryName; ?></h2>

	
			<?php foreach($catass as $catassid): ?>

				<div class="product">
					<h3>
					<?php foreach($products as $product): ?>
						<?php echo anchor('products/'.$product->pkProductId, $product->Name); ?>
						<br/>
					<?php endforeach; ?>
						(<?php echo $products->count(); ?> Products)
					</h3>

					<div class="description">
						<?php echo $category->SEODescription; ?>
					</div>
				</div>

			<?php endforeach; ?>

		</div>

	<?php endforeach; ?>

</div>

</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Category Creation Form</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css" type="text/css" media="all">
</head>
<body>

<div id="create_product">

	<p class="heading">New Category Creation</p>
	 <?php echo form_open('create_category/submit'); ?>
	 <?php echo validation_errors('<p class="error">','</p>'); ?>
	<p>
		<label for="publish">Published?: </label>
		<?php echo form_checkbox('publish'); ?>
	</p>
	<p>
		<label for="name">Category Name: </label>
		<?php echo form_input('name'); ?>
	</p>
	<p>
		<label for="srno">SrNo: </label>
		<?php echo form_input('srno'); ?>
	</p>
	<p>
		<?php echo form_submit('submit','Create this category'); ?>
	</p>

	<?php echo form_close(); ?>


</div>

</body>
</html>

	<div class="categories">

	<h1><?php echo $category->CategoryName; ?></h1>

	

		<div class="category">

	
			<div class="description"><!--category description -->
						<?php echo $category->SEODescription; ?>
					</div>
					<h2>Products:</h2><!-- product list -->
					<?php foreach($products as $product): ?>
				<div class="product">
					<h3>
						<!-- call the products controller in the links, passing required vars -->
						<?php echo anchor('products/display/'.$product->pkProductId, $product->Name); ?>
						
					</h3>

					<div class="description"><!-- Product Description -->
						<?php echo $product->SEODescription; ?>
					</div>
				</div>
				<?php endforeach; ?><!-- product loop end -->


		</div>

	

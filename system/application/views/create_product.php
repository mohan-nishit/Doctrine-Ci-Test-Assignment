<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Product Creation Form</title>
	<link rel="stylesheet" href="<?php echo base_url(); ?>css/style.css" type="text/css" media="all">
</head>
<body>

<div id="create_product">

	<p class="heading">New Product Creation</p>
	<?php $options = Doctrine::getTable('CategoryTable')->findAll(); 
		foreach($options as $option){
			$name = $option->CategoryName;
			$id = $option->pkCatId;
			$choices[$id] = $name;
		} ?>
	 <?php echo form_open('create_product/submit'); ?>
	 <?php echo validation_errors('<p class="error">','</p>'); ?>
	<p>
		<label for="publish">Published?: </label>
		<?php echo form_checkbox('publish'); ?>
	</p>
	<p>
		<label for="name">Product Name: </label>
		<?php echo form_input('name'); ?>
	</p>
	<p>
		<label for="sku">SKU: </label>
		<?php echo form_input('sku'); ?>
	</p>
	<p>
		<label for="brief_description">Brief Description: </label>
		<?php echo form_input('brief_description'); ?>
	</p>
	<p>
		<label for="cat_name">Choose Category: </label>
		<?php echo form_dropdown('cat_name',$choices); ?>
	</p>
	<p>
		<?php echo form_submit('submit','Create this product'); ?>
	</p>

	<?php echo form_close(); ?>


</div>

</body>
</html>

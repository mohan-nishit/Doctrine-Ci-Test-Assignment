<?php
class ProdCatAssTable extends Doctrine_Record {
	//define the table
	public function setTableDefinition() {
		$this->hasColumn('pkProdCatAssId', 'integer', 5,array('type' => 'integer', 'length' => 8, 'primary' => true, 'autoincrement' => true));
		$this->hasColumn('fkProductId', 'integer', 15);
		$this->hasColumn('fkCategoryId', 'integer', 15);
		$this->hasColumn('SrNo', 'integer', 15);
	}
	//add timestamp and table name
	public function setUp() {
		$this->setTableName('ProdCatAssTable');
		//1-to-many relation with products
		$this->hasMany('ProductTable as Products', array(
            'local' => 'fkProductId',
            'foreign' => 'pkProductId'
        ));
		
	}
}

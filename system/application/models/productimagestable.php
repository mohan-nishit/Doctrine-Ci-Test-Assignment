<?php
class ProductImagesTable extends Doctrine_Record {
	//define the table
	public function setTableDefinition() {
		$this->hasColumn('pkProdImageAss', 'integer', 5,array('type' => 'integer', 'length' => 8, 'primary' => true, 'autoincrement' => true));
		$this->hasColumn('fkProductId', 'integer', 15);
		$this->hasColumn('ImageFileName', 'string', 255);
		$this->hasColumn('SrNo', 'integer', 15);
	}
	//add timestamp and table name
	public function setUp() {
		$this->setTableName('Images');
		//many-to-1 relation with products table
		$this->hasOne('ProductTable', array(
            'local' => 'fkProductId',
            'foreign' => 'pkProductId'
        ));
	}
}

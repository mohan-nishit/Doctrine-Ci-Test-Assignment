<?php
class ProductTable extends Doctrine_Record {
	//define the table
	public function setTableDefinition() {
		$this->hasColumn('pkProductId', 'integer', 5,array('type' => 'integer', 'length' => 8, 'primary' => true, 'autoincrement' => true));
		$this->hasColumn('Publish', 'boolean');
		$this->hasColumn('SEOTitle', 'string', 255);
		$this->hasColumn('SEODescription', 'string', 255);
		$this->hasColumn('SEOKeywords', 'string', 255);
		$this->hasColumn('Name', 'string', 255);
		$this->hasColumn('SKU', 'integer', 15, array('unique' => 'true'));
		$this->hasColumn('Brief_Description', 'string', 255);
	}
	//add timestamp and table name and relations
	public function setUp() {
		$this->setTableName('product');
		$this->actAs('Timestampable');
		//one-to-many relation with images table
		$this->hasMany('ProductImagesTable as Images', array(
            'local' => 'pkProductId',
            'foreign' => 'fkProductId'
        ));
		//one-to-one relation with catass table
		$this->hasOne('ProdCatAssTable', array(
            'local' => 'pkProductId',
            'foreign' => 'fkProductId'
        ));
	}
}

	/*public function getProductsArray() {

		$threads = Doctrine_Query::create()
			->select('t.title')
			->addSelect('p.id, (COUNT(p.id) - 1) as num_replies')
			->addSelect('MIN(p.id) as first_post_id')
			->from('Thread t, t.Posts p')
			->where('t.forum_id = ?', $this->id)
			->groupBy('t.id')
			->setHydrationMode(Doctrine::HYDRATE_ARRAY)
			->execute();

		foreach ($threads as &$thread) {

			$post = Doctrine_Query::create()
				->select('p.created_at, u.username')
				->from('Post p, p.User u')
				->where('p.id = ?', $thread['Posts'][0]['first_post_id'])
				->setHydrationMode(Doctrine::HYDRATE_ARRAY)
				->fetchOne();

			$thread['num_replies'] = $thread['Posts'][0]['num_replies'];
			$thread['created_at'] = $post['created_at'];
			$thread['username'] = $post['User']['username'];
			$thread['user_id'] = $post['User']['id'];
			unset($thread['Posts']);

		}

		return $threads;

	}
*/

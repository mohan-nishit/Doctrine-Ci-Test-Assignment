<?php
class CategoryTable extends Doctrine_Record {
	//define the table
	public function setTableDefinition() {
		$this->hasColumn('pkCatId', 'integer', 5,array('type' => 'integer', 'length' => 8, 'primary' => true, 'autoincrement' => true));
		$this->hasColumn('Publish', 'boolean');
		$this->hasColumn('SEOTitle', 'string', 255);
		$this->hasColumn('SEODescription', 'string', 255);
		$this->hasColumn('SEOKeywords', 'string', 255);
		$this->hasColumn('CategoryName', 'string', 255);
		$this->hasColumn('SrNo', 'integer', 5);
	}
	//add timestamp and table name
	public function setUp() {
		$this->setTableName('Categories');
		//1-to-1 relation with catass table
		$this->hasOne('ProdCatAssTable', array(
            'local' => 'pkCatId',
            'foreign' => 'fkCategoryId'
        ));
	}
}
